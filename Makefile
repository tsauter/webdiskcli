
NAME=webdiskcli
VERSION=0.1
ifeq ($(OS),Windows_NT)
	DATE=
	BUILDUSER=$(USERNAME)
else
	DATE=$(shell date +%FT%T%z)
	BUILDUSER=$(USER)
endif
HOSTNAME=$(shell hostname)
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.BuildTime=${DATE} -X main.BuildHost=${HOSTNAME} -X main.BuildUser=${BUILDUSER}"
MODULES=main.go

run:
	go run ${LDFLAGS} ${MODULES}

install: test
	go install ${LDFLAGS} -v

build: test
	go build ${LDFLAGS} -v -o ${NAME}.exe ${MODULES}

fmt:
	go fmt

test:
	go test

# https://github.com/golang/lint
lint:
	golint

# https//godoc.org/golang.org/x/tools/cmd/vet
vet:
	go vet

godep:
	godep save

clean:
	del ${NAME}.exe

distclean: clean

