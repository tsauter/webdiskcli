FROM debian:jessie
MAINTAINER Thorsten Sauter

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y curl git

RUN curl https://storage.googleapis.com/golang/go1.7.3.linux-amd64.tar.gz --output /go1.7.3.linux-amd64.tar.gz --silent && tar -C /usr/local -xzf /go1.7.3.linux-amd64.tar.gz && ln -s /usr/local/go/bin/go /usr/local/bin/go && rm /go1.7.3.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin

ENV GOPATH=/goenv
WORKDIR /webdisk
RUN mkdir -p /goenv

RUN go get github.com/urfave/cli
RUN cd /goenv/src && git clone https://gitlab.com/tsauter/webdiskcli.git
RUN go install webdiskcli

ENTRYPOINT ["/goenv/bin/webdiskcli"]
CMD ["-directory=/webdisk/data", "-userdb-file=/webdisk/etc/users.json"]

VOLUME /webdisk

