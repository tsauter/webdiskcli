# Webdisk

## Basic Concept

The Webdisk is a webservice for file sharing through the Intranet. Each file is
identified through an unique magic code. Without this magic code no download is
possible.

## Interfaces

The following HTTP endpoints are available:

 - /upload
   the file is uploaded through a POST request

 - /download/<magiccode>
   the file will be downloaded through the specified magic code

 - /login

 - /logoff

 - /content


