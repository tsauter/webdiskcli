package main

import (
	"errors"
	"fmt"
	"github.com/urfave/cli"
	"os"
	"webdisk/AuthManager"
	"webdisk/FileManager"
)

var (
	fileDirectory string
	userdbFile    string
)

func cleanupDirectory(c *cli.Context) error {
	// load file backend
	filer, err := FileManager.New(fileDirectory)
	if err != nil {
		return errors.New(fmt.Sprintf("Initializing FileManager failed: %s", err.Error()))
	}

	fmt.Printf("File directory: %s\n", filer.GetStorageDir())

	count, err := filer.DeleteExpiredFiles()
	if err != nil {
		return err
	}

	fmt.Printf("%d file(s) deleted.\n", count)

	return nil
}

func createUser(c *cli.Context) error {
	if len(c.Args()) != 2 {
		return errors.New(fmt.Sprintf("Invalid count of arguments: %s\n", c.Args()))
	}

	username := c.Args()[0]
	password := c.Args()[1]

	authMgr, err := AuthManager.New(userdbFile)
	if err != nil {
		return errors.New(fmt.Sprintf("Initializing AuthManager failed: %s", err.Error()))
	}

	if _, err := authMgr.CreateUser(username, password, false); err != nil {
		return errors.New(fmt.Sprintf("User creation failed: %s: %s\n", username, err.Error()))
	}

	fmt.Printf("User %s succesfully created.\n", username)

	return nil
}

func main() {
	app := cli.NewApp()
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "directory",
			Value:       "./files",
			Usage:       "Location of uploaded files.",
			Destination: &fileDirectory,
		},
		cli.StringFlag{
			Name:        "userdb-file",
			Value:       "users.json",
			Usage:       "Location of User/password file.",
			Destination: &userdbFile,
		},
	}
	app.Commands = []cli.Command{
		{
			Name:   "cleanup",
			Action: cleanupDirectory,
		},
		{
			Name:   "createuser",
			Action: createUser,
		},
	}
	app.Run(os.Args)

	return
}
